###
# Global Variables
###

# Database

variable "db-instance-identifier" {
  default     = "terraform-datasource"
  description = "The Database Instance Identifier"
  type        = string
}

variable "db-instance-name" {
  default     = "terraform_db"
  description = "The Database Instance Name"
  type        = string
}

variable "db-instance-class" {
  default     = "db.t2.micro"
  description = "The Database Instance Class"
  type        = string
}

variable "db-instance-subnet-group" {
  default     = "default-vpc-89b5def4"
  description = "The Database Instance Subnet"
  type        = string
}

variable "db-instance-username" {
  default     = "root"
  description = "The Database Instance Username"
  type        = string
}

variable "db-instance-password" {
  default     = "12qwaszx"
  description = "The Database Instance Password"
  type        = string
}

variable "db-instance-availability-zone" {
  default     = "us-east-1a"
  description = "The Database Availability Zone"
  type        = string
}

variable "db-instance-engine-version" {
  default     = "5.7"
  description = "The Database Engine Version"
  type        = string
}


# Security

variable "security-vpc-id" {
  default     = "vpc-89b5def4"
  description = "The VPC ID"
  type        = string
}

variable "security-group-protocol" {
  default     = "-1"
  description = "The Security Group Protocol"
  type        = string
}

variable "security-group-cidr-block" {
  default     = "0.0.0.0/0"
  description = "The Security Group Cidr Block"
  type        = string
}

variable "security-group-port" {
  default     = 0
  description = "The Security Group Cidr Block"
  type        = number
}


# EC2

variable "ec2-instance-availability-zone" {
  default     = "us-east-1a"
  description = "The EC2 Instance Availability Zone"
  type        = string
}

variable "ec2-instance-type" {
  default     = "t2.micro"
  description = "The EC2 Instance Type"
  type        = string
}

variable "ec2-instance-ami" {
  default     = "ami-0c2b8ca1dad447f8a"
  description = "The EC2 Instance AMI"
  type        = string
}

variable "ec2-instance-subnet-id" {
  default     = "subnet-670b5501"
  description = "The EC2 Instance AMI"
  type        = string
}

variable "ec2-instance-key-name" {
  default     = "ac-academy"
  description = "The EC2 Instance Key Pair Name"
  type        = string
}


# Credentials

variable "access_key" {
  type = string
}

variable "secret_key" {
  type = string
}
