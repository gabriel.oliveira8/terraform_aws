###
# RDS Database Instances
###

resource "aws_db_instance" "datasource" {
  allocated_storage          = 20
  engine                     = "mysql"
  engine_version             = var.db-instance-engine-version
  instance_class             = var.db-instance-class
  name                       = var.db-instance-name
  identifier                 = var.db-instance-identifier
  username                   = var.db-instance-username
  password                   = var.db-instance-password
  parameter_group_name       = "default.mysql5.7"
  skip_final_snapshot        = true
  publicly_accessible        = true
  db_subnet_group_name       = var.db-instance-subnet-group
  multi_az                   = false
  auto_minor_version_upgrade = true
  availability_zone          = var.db-instance-availability-zone
  vpc_security_group_ids     = [aws_security_group.public.id]

  tags = {
    Name = "datasource_instance"
  }
}
