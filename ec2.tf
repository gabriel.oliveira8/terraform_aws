###
# EC2 Instances
###

resource "aws_instance" "ec2" {
  ami                    = var.ec2-instance-ami
  instance_type          = var.ec2-instance-type
  availability_zone      = var.ec2-instance-availability-zone
  key_name               = var.ec2-instance-key-name
  count                  = 1
  vpc_security_group_ids = [aws_security_group.public.id]
  subnet_id              = var.ec2-instance-subnet-id

  tags = {
    Name = "ec2_instance"
  }
}
