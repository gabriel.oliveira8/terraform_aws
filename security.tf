###
# Public Security Groups
###

resource "aws_security_group" "public" {
  name        = "terraform-security-group"
  description = "Public internet access"
  vpc_id      = var.security-vpc-id

  tags = {
    Name = "public_security_group"
  }
}

resource "aws_security_group_rule" "public_out" {
  from_port         = var.security-group-port
  protocol          = var.security-group-protocol
  security_group_id = aws_security_group.public.id
  to_port           = var.security-group-port
  type              = "egress"
  cidr_blocks       = [var.security-group-cidr-block]
}

resource "aws_security_group_rule" "public_in" {
  from_port         = var.security-group-port
  protocol          = var.security-group-protocol
  security_group_id = aws_security_group.public.id
  to_port           = var.security-group-port
  type              = "ingress"
  cidr_blocks       = [var.security-group-cidr-block]
}
