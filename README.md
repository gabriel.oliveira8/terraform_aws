<div align="center">
    <h1>Terraform AWS</h1>
</div>

# :question: About
This project will be used as part of the DevOps course powered by AC Academy, it implements a Terraform script to create an infrastructure as code on AWS.

The objective here is to put in practice DevOps skills learned during the course.

# :computer: Technologies
- Terraform
- Docker
- AWS
- Cloud Computing
- IaaS
- IaaC

# :memo: License
This project is under MIT License, see [LICENSE](LICENSE) file for more information.
